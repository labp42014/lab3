struct nodoUsuariosFalsos
{
	string 			ci;
	DTInfoLogueo	infoLogueo;
	nodoMapaCI*		sig;
};

typedef usuariosFalsos nodoUsuariosFalsos*;

class UsuariosFalsos
{
private:
	usuariosFalsos usuarios;

public:
	DTInfoLogueo	find(ci);
	void			add(ci,DTInfoLogueo);
};