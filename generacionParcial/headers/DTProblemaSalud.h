#include <string>

class DTProblemaSalud
//DataType
{
private:
	int		codigoPS;
	string	etiqueta;

public:
	//Creadoras
	DTProblemaSalud(int codigoPS,string etiqueta);

	//Getters
	int		getCodigoPS();
	string	getEtiqueta();

	//Setters
	void	setCodigoPS(int codigoPS);
	string	setEtiqueta(string nombre);
};