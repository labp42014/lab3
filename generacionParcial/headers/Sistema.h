#ifndef SISTEMA_H_
#define SISTEMA_H_

#include <string>

#include "Rol.h"

using namespace std;

class Sistema
{
private:
	//Memoria Alta Usuario
	string	ci;
	string	nombre;
	string	apellido;
	Sexo	sexo;
	Fecha	fechaNac;
	Rol roles[2];

	//Memoria Iniciar Sesion
	string	ci;

	//Memoria Registro de consulta.
public:
	//Operaciones Iniciar Sesion
	DTInfoLogueo	iniciar(string ci);
	bool			ingresarContrasenia(string contrasenia);
	void			asignarSesionAUsuario();
	bool			crearContrasenia(string contrasenia);


	//Operaciones AltaReactivacion
	bool			iniciarAltaReactivacion(string ci);
	void			ingresarDatos(string nombre, string apellido, Sexo sexo, Fecha fechaNac, SetRoles roles);
	void			altaUsuario();
	DTDatosUsuario	pedirDatos();
	void			reactivarUsuario();

	//Operaciones RegistroConsulta
	void			registrarConsultaComun(string ciMedico,string ciSocio,Fecha fecha);
};

#endif /* SISTEMA_H_ */