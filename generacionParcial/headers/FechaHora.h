/*
 * FechaHora.h
 *
 *  Created on: 03/05/2014
 *      Author: matias
 */

#ifndef FECHAHORA_H_
#define FECHAHORA_H_

class Fecha
//DataType Fecha
{
private:
	int	dia;
	int	mes;
	int	anio;

public:
	//Creadores
	Fecha(int dia,int mes,int anio);

	//Getters
	int getDia();
	int getMes();
	int getAnio();
};

class Hora
//DataType Hora
{
private:
	int	horas;
	int	minutos;

public:
	//Creadoras
	Hora(int horas,int minutos);

	//Getters
	int getHoras();
	int getMinutos();
};


#endif /* FECHAHORA_H_ */
