/*
 * FechaHora.cpp
 *
 *  Created on: 03/05/2014
 *      Author: matias
 */

#include "../headers/FechaHora.h"


// --------------- Fecha --------------- //
//Creadoras
Fecha::Fecha(int dia,int mes,int anio): dia(dia),mes(mes),anio(anio) { }

//Getters
int Fecha::getDia() { return dia; }
int Fecha::getMes() { return mes; }
int Fecha::getAnio() { return anio; }


// --------------- Hora --------------- //
//Creadoras
Hora::Hora(int horas,int minutos): horas(horas), minutos(minutos) { }

//Getters
int Hora::getHoras() { return horas; }
int Hora::getMinutos() { return minutos; }
