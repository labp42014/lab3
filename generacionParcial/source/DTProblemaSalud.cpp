#include <string>

#include "../headers/DTProblemaSalud"

//Constructoras
DTProblemaSalud::DTProblemaSalud(int codigoPS,string etiqueta): codigoPS(codigoPS), etiqueta(etiqueta) { }

//Getters
int		getCodigoPS() { return codigoPS; }
string	getEtiqueta() { return etiqueta; }

//Setters
void	setCodigoPS() { this->codigoPS=codigoPS; }
void	setEtiqueta() { this->etiqueta=etiqueta; }